<?php

namespace frontend\controllers;

use common\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;

class ChannelController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['suscribe'],
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ],
        ];
    }

    public function actionView($username)
    {
       $channel = $this->findChannel($username);

        return $this->render('view', [
            'channel' => $channel
        ]);
    }

    public function actionSuscribe($username)
    {
        $channel = $this->findChannel($username);

    }

    protected function findChannel($username)
    {
        $channel = User::findByUsername($username);
        if(!$channel){
            throw new NotFoundHttpException("Channel does not exist.");
        }
        return $channel;
    }


}




?>