<?php

use yii\widgets\ListView;

?>

<?php echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_video_item',
    'layout' => '<div class="d-flex">{items}</div>{pager}',
    'itemOptions' => [
        'tag' => false
    ]
]) ?>