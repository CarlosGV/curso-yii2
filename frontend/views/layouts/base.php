<?php

/* @var $this \yii\web\View */
/* @var $content string */

use frontend\assets\AppAsset;
use yii\helpers\Html;

use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
// NOTA: Es importante agregar el assetManager en el archivo
// main.php de la carpeta config para que se pueda leer el css 
// que tenemos en la carpeta web/css, al igual si agregamos un
// archivo .js
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">

<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css"
        rel="stylesheet">
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>

    <div class="wrap h-100 d-flex flex-column">
        <div class="wrap h-100 d-flex flex-column">
            <?php echo $this->render('_header') ?>

            <?php echo $content ?>
        </div>

        <?php $this->endBody() ?>
</body>

</html>
<?php $this->endPage() ?>