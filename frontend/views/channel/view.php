<?php

/** @var $channel \commmon\models\User */

use yii\helpers\Url;
use yii\widgets\Pjax;

?>
<div class="jumbotron">
    <h1 class="display-4"><?php echo $channel->username ?></h1>
    <hr class="my-4">
    <?php Pjax::begin()?>
        <?php echo $this->render('_suscribe',[
            'channel' => $channel
        ]) ?>
    <?php Pjax::end()?>
</div>