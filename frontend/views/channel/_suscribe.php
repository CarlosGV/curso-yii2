<?php

use yii\helpers\Url;

?>

<a class="btn btn-danger" href="<?php echo Url::to(['channel/suscribe', 
        'username' => $channel->username]) ?>" 
        data-method="post">
        Suscribe <i class="far fa-bell"></i>
</a>9