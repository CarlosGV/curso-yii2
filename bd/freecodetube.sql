-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-02-2021 a las 17:16:40
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.3.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `freecodetube`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1612886614),
('m130524_201442_init', 1612886620),
('m190124_110200_add_verification_token_column_to_user_table', 1612886620),
('m210209_224821_create_videos_table', 1612911574);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT 10,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `verification_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `verification_token`) VALUES
(1, 'test', 'X0i-X-zSAyQJ1gYTU1z1oJjTOsaRtf3O', '$2y$13$aJ.CdBsAgMmMw224hvXON.BBECEbm57o6H1BXoSsTlvcC.tLIe0ey', NULL, 'test@example.com', 10, 1612886717, 1612887500, 'yimhprDX_-M1LIKoGG6-tqQ7Jg9rU19D_1612886717'),
(2, 'admin', 'Jj5gPRLkQ5M0b2H03wTlaw0DC_YlU9-2', '$2y$13$B9jEETImWrnYKsm.LEP/deHK91M8XQZDN9GZGiRLfXz2wnIuCQV06', NULL, 'admin@lubtrac.com', 10, 1612887565, 1612887641, '_4ibrsTGnL0ZUs__m3TJGMqYV9ejNsCk_1612887565'),
(3, 'vic', 'QGhp0H_m9un42T6ag-HaIg7O7bM4AYxN', '$2y$13$Ow7MQWrCRQUJZSOfxn8d1O1oevbkDa3of8UUWzjS5PEkVWo58/qCu', NULL, 'vic@test.com', 10, 1613053981, 1613053981, 'yBidtFFEKFlZ5XZ4qEOttxftpFejPiTl_1613053981');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `videos`
--

CREATE TABLE `videos` (
  `video_id` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tags` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  `has_thumbnail` tinyint(1) DEFAULT NULL,
  `video_name` varchar(512) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL,
  `updated_at` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `thumbnail` varchar(512) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `videos`
--

INSERT INTO `videos` (`video_id`, `title`, `description`, `tags`, `status`, `has_thumbnail`, `video_name`, `created_at`, `updated_at`, `created_by`, `thumbnail`) VALUES
('3auFQXCE', 'Lithium - Nirvana.mp4', '', 'curco,php,yii', 0, 1, 'Lithium - Nirvana.mp4', 1612997205, 1612998897, 2, ''),
('M6uKyELr', 'Return to Serenity - Testament.mp4', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Ducimus labore vel assumenda aliquid fuga iusto asperiores alias, modi veritatis perferendis quae nostrum placeat repellat veniam dicta doloribus explicabo, voluptate natus.', '', 1, 1, 'Return to Serenity - Testament.mp4', 1613060739, 1613157952, 3, ''),
('wOgfl_CJ', 'Smells Like Teen Spirit - Nirvana.mp4', '', '', 1, 1, 'Smells Like Teen Spirit - Nirvana.mp4', 1612987765, 1612996127, 2, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video_like`
--

CREATE TABLE `video_like` (
  `id` int(11) NOT NULL,
  `video_id` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `type` int(1) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `video_like`
--

INSERT INTO `video_like` (`id`, `video_id`, `user_id`, `type`, `created_at`) VALUES
(9, 'wOgfl_CJ', 2, 1, 1613157596),
(10, 'M6uKyELr', 2, 1, 1613157606),
(15, 'M6uKyELr', 3, 0, 1613157646);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video_view`
--

CREATE TABLE `video_view` (
  `id` int(11) NOT NULL,
  `video_id` varchar(16) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `video_view`
--

INSERT INTO `video_view` (`id`, `video_id`, `user_id`, `created_at`) VALUES
(1, 'wOgfl_CJ', NULL, 1613140146),
(2, 'wOgfl_CJ', NULL, 1613140195),
(3, 'wOgfl_CJ', NULL, 1613141166),
(4, 'wOgfl_CJ', NULL, 1613141168),
(5, 'wOgfl_CJ', NULL, 1613141232),
(6, 'M6uKyELr', NULL, 1613141811),
(7, 'wOgfl_CJ', NULL, 1613145952),
(8, 'wOgfl_CJ', NULL, 1613145960),
(9, 'wOgfl_CJ', 2, 1613152124),
(10, 'M6uKyELr', 2, 1613152140),
(11, 'M6uKyELr', 2, 1613152186),
(12, 'M6uKyELr', 2, 1613152238),
(13, 'M6uKyELr', 2, 1613152324),
(14, 'wOgfl_CJ', 2, 1613152395),
(15, 'wOgfl_CJ', 2, 1613152654),
(16, 'wOgfl_CJ', 2, 1613153909),
(17, 'wOgfl_CJ', 2, 1613154018),
(18, 'wOgfl_CJ', 2, 1613154038),
(19, 'wOgfl_CJ', 2, 1613154430),
(20, 'wOgfl_CJ', 2, 1613154469),
(21, 'wOgfl_CJ', 2, 1613154477),
(22, 'wOgfl_CJ', 2, 1613154480),
(23, 'M6uKyELr', 2, 1613154722),
(24, 'M6uKyELr', 2, 1613155021),
(25, 'M6uKyELr', 2, 1613156023),
(26, 'M6uKyELr', 2, 1613156890),
(27, 'M6uKyELr', 2, 1613157333),
(28, 'wOgfl_CJ', 2, 1613157590),
(29, 'wOgfl_CJ', 2, 1613157601),
(30, 'M6uKyELr', 2, 1613157604),
(31, 'wOgfl_CJ', 2, 1613157608),
(32, 'M6uKyELr', 2, 1613157610),
(33, 'M6uKyELr', 3, 1613157635),
(34, 'M6uKyELr', 3, 1613157934),
(35, 'M6uKyELr', 3, 1613157955),
(36, 'M6uKyELr', 3, 1613402977);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- Indices de la tabla `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`video_id`),
  ADD KEY `idx-videos-created_by` (`created_by`);

--
-- Indices de la tabla `video_like`
--
ALTER TABLE `video_like`
  ADD PRIMARY KEY (`id`),
  ADD KEY `video_id` (`video_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indices de la tabla `video_view`
--
ALTER TABLE `video_view`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idx-video_view-video_id` (`video_id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `video_like`
--
ALTER TABLE `video_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `video_view`
--
ALTER TABLE `video_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `videos`
--
ALTER TABLE `videos`
  ADD CONSTRAINT `fk-videos-created_by` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Filtros para la tabla `video_like`
--
ALTER TABLE `video_like`
  ADD CONSTRAINT `video_like_ibfk_1` FOREIGN KEY (`video_id`) REFERENCES `videos` (`video_id`),
  ADD CONSTRAINT `video_like_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`);

--
-- Filtros para la tabla `video_view`
--
ALTER TABLE `video_view`
  ADD CONSTRAINT `video_view_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  ADD CONSTRAINT `video_view_ibfk_2` FOREIGN KEY (`video_id`) REFERENCES `videos` (`video_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
